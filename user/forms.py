from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from user.models import UserProfile


class SignUpForm(UserCreationForm):
    username = forms.CharField(max_length=30,  label='Nome de usuário')
    email = forms.EmailField(max_length=100, label='E-mail')
    first_name = forms.CharField(
        max_length=100,  label='Nome')
    last_name = forms.CharField(
        max_length=100,  label='Sobrenome')

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name',
                  'last_name', 'password1', 'password2')
        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'username'}),
            'email': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'username'}),
        }


class UserUpdateForm(UserChangeForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name')
        widgets = {
            'username': forms.TextInput(attrs={'class': 'input', 'placeholder': 'username'}),
            'email': forms.EmailInput(attrs={'class': 'input', 'placeholder': 'email'}),
            'first_name': forms.TextInput(attrs={'class': 'input', 'placeholder': 'first_name'}),
            'last_name': forms.TextInput(attrs={'class': 'input', 'placeholder': 'last_name'}),
        }


CITY = [
    ('São Paulo', 'São Paulo'),
    ('Rio de Janeiro', 'Rio de Janeiro'),
    ('Parelhas', 'Parelhas'),
]


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('phone', 'address', 'city')
        widgets = {
            'phone': forms.TextInput(attrs={'class': 'input', 'placeholder': 'phone'}),
            'address': forms.TextInput(attrs={'class': 'input', 'placeholder': 'address'}),
            'city': forms.Select(attrs={'class': 'input', 'placeholder': 'city'}, choices=CITY),
            'first_name': forms.TextInput(attrs={'class': 'input', 'placeholder': 'first_name'}),
            #  'image': forms.FileInput(attrs={'class': 'input', 'placeholder': 'image'}),
        }
