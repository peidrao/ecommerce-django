from django.shortcuts import (render, HttpResponse, HttpResponseRedirect)
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.contrib import messages
import json

# Create your views here.
from .models import (Setting, ContactMessage, FAQ)
from .forms import ContactMessageForm, SearchForm


from product.models import Category, Product, Images, Comment, Variants
from order.models import ShopCart


def index(request):
    setting = Setting.objects.get(pk=1)
    category = Category.objects.all()
    current_user = request.user
    shopcart = ShopCart.objects.filter(user_id=current_user.id)
    product_latest = Product.objects.all().order_by(
        '-id')[:5]  # Last 5 products
    product_random = Product.objects.all().order_by(
        '?')[:5]  # Random selected products

    total = 0
    for product in shopcart:
        total = total + product.quantity

    print(total)

    page = 'home'
    context = {'setting': setting, 'page': page,
               'product_latest': product_latest, 'product_random': product_random, 'category': category, 'total': total}
    return render(request, 'index.html', context)


def aboutus(request):
    setting = Setting.objects.get(pk=1)
    context = {'setting': setting}
    return render(request, 'pages/about.html', context)


def contact(request):
    if request.method == 'POST':  # Check method
        form = ContactMessageForm(request.POST)
        if form.is_valid():
            data = ContactMessage()  # Create relation with model

            data.name = form.cleaned_data['name']  # Get from input data
            data.email = form.cleaned_data['email']
            data.subject = form.cleaned_data['subject']
            data.message = form.cleaned_data['message']
            data.ip = request.META.get('REMOTE_ADDR')

            data.save()  # Save data to table

            messages.success(request, 'Your message has ben sent. Thank you!')
            return HttpResponseRedirect('/contact')

    setting = Setting.objects.get(pk=1)
    form = ContactMessageForm
    context = {'setting': setting, 'form': form}
    return render(request, 'pages/contact.html', context)


def category_products(request, id, slug):
    category = Category.objects.all()
    products = Product.objects.filter(category_id=id)
    product_latest = Product.objects.all().order_by(
        '-id')[:3]  # Last 5 products
    context = {
        'products': products,  'category': category, 'product_latest': product_latest}

    return render(request, 'pages/category_products.html', context)


def search(request):
    if request.method == 'POST':  # check post
        form = SearchForm(request.POST)
        if form.is_valid():
            query = form.cleaned_data['query']  # get form input data
            # catid = form.cleaned_data['catid']
            products = Product.objects.filter(
                title__icontains=query)

            category = Category.objects.all()
            context = {'products': products, 'query': query,
                       'category': category}
            return render(request, 'pages/search_products.html', context)

    return HttpResponseRedirect('/')


def search_auto(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        products = Product.objects.filter(title__icontains=q)
        results = []
        for rs in products:
            product_json = rs.title
            results.append(product_json)
        data = json.dumps(results)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)


def product_detail(request, id, slug):
    query = request.GET.get('q')
    category = Category.objects.all()

    product = Product.objects.get(pk=id)

    images = Images.objects.filter(product_id=id)
    comments = Comment.objects.filter(product_id=id, status='True')
    context = {'product': product, 'category': category,
               'images': images, 'comments': comments,
               }
    if product.variant != "None":  # Product have variants
        if request.method == 'POST':  # if we select color
            variant_id = request.POST.get('variantid')
            variant = Variants.objects.get(id=variant_id)
            colors = Variants.objects.filter(
                product_id=id, size_id=variant.size_id)
            sizes = Variants.objects.raw(
                'SELECT * FROM product_variants WHERE product_id=%s GROUP BY size_id', [id])
            query += variant.title+' Size:' + \
                str(variant.size) + ' Color:' + str(variant.color)
        else:
            variants = Variants.objects.filter(product_id=id)
            colors = Variants.objects.filter(
                product_id=id, size_id=variants[0].size_id)
            sizes = Variants.objects.raw(
                'SELECT * FROM product_variants WHERE product_id=%s GROUP BY size_id', [id])
            variant = Variants.objects.get(id=variants[0].id)
        context.update({'sizes': sizes, 'colors': colors,
                        'variant': variant, 'query': query
                        })
    return render(request, 'pages/product_detail.html', context)


def ajaxcolor(request):
    data = {}
    if request.POST.get('action') == 'POST':
        size_id = request.POST.get('size')
        productid = request.POST.get('productid')
        colors = Variants.objects.filter(product_id=productid, size_id=size_id)
        context = {
            'size_id': size_id,
            'productid': productid,
            'colors': colors,
        }
        data = {'rendered_table': render_to_string(
            'pages/color_list.html', context=context)}
        return JsonResponse(data)
    return JsonResponse(data)


def faq(request):
    category = Category.objects.all()
    faq = FAQ.objects.filter(status="True").order_by('ordernumber')
    context = {
        'category': category,
        'faq': faq
    }
    return render(request, 'pages/faq.html', context)
