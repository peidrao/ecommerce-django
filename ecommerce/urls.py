from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

from home import views
from order import views as OrderViews
from user import views as UserViews

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('home.urls')),
    path('home/', include('home.urls')),
    path('product/', include('product.urls')),
    path('order/', include('order.urls')),
    path('user/', include('user.urls'), name="user"),
    path('ckeditor/', include('ckeditor_uploader.urls')),

    path('about/', views.aboutus, name='aboutus'),
    path('contact/', views.contact, name='contact'),

    path('search/', views.search, name='search'),
    path('search_auto/', views.search_auto, name='search_auto'),
    path('ajaxcolor/', views.ajaxcolor, name='ajaxcolor'),
    path('faq/', views.faq, name='faq'),

    path('category/<int:id>/<slug:slug>',
         views.category_products, name='category_products'),
    path('product/<int:id>/<slug:slug>',
         views.product_detail, name='product_detail'),

    path('shopcart/', OrderViews.shopcart, name='shopcart'),

    path('shopcart/', OrderViews.shopcart, name='shopcart'),

    path('login/', UserViews.login_form, name='login_form'),
    path('signup/', UserViews.signup_form, name='signup_form'),
    path('logout/', UserViews.logout_func, name='logout_func'),
    path('profile/', UserViews.profile, name='profile'),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
