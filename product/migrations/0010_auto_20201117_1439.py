# Generated by Django 3.1.2 on 2020-11-17 14:39

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0009_auto_20201117_1239'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='detailt',
            field=ckeditor_uploader.fields.RichTextUploadingField(),
        ),
    ]
