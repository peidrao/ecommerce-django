from django.shortcuts import (render, HttpResponse, HttpResponseRedirect)
from django.contrib import messages

from .models import Comment, CommentForm
# Create your views here.


def index(request):
    return HttpResponse('Hello')


def add_comment(request, id):
    url = request.META.get('HTTP_REFERER')
    print('Teste1')
    if request.method == 'POST':
        form = CommentForm(request.POST)
        print('Teste2')
        print(form)
        if form.is_valid():
            print('Teste3')
            data = Comment()
            data.subject = form.cleaned_data['subject']
            data.comment = form.cleaned_data['comment']
            data.rate = form.cleaned_data['rate']
            data.ip = request.META.get('REMOTE_ADDR')
            data.product_id = id

            current_user = request.user
            data.user_id = current_user.id
            data.save()

            messages.success(
                request, 'Sua avaliação foi enviada com sucesso.')
            return HttpResponseRedirect(url)
    return HttpResponseRedirect(url)
