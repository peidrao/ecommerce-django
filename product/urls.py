from django.urls import path, include

from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('addcomment/<int:id>', views.add_comment, name='add_comment')
]
